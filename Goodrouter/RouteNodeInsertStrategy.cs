internal enum RouteNodeInsertStrategy
{
    Intermediate,
    Merge,
    AddToThis,
    AddToOther,
}
